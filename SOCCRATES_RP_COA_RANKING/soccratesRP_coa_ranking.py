#!/usr/bin/env python3
# encoding: utf-8
import datetime
import json
import time

from cortexutils.analyzer import Analyzer
import requests


def logprint(message,data):
    date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    filelog = open("/var/log/cortex_analyzers/cortex_SOCCRATES_RP_Coa_ranking.log", "a")
    filelog.write(f"[{date}][{message}]{data}\n")
    filelog.close()


class SOCCRATES_RP_COA_RANKING_ANALYZER(Analyzer):
    def __init__(self):
        Analyzer.__init__(self)
        self.service = self.get_param('config.service', 'coa_ranking',
                                      'Service parameter is missing')
        self.responsePlannerApi_url = self.get_param('config.responsePlannerApi_url',
                                                     'http://rp-backend:8000/',
                                                     'Missing URL of the Response Planner API')
        self.service_url = self.get_param('config.coa_ranking_url',
                                          'coa_ranking',
                                          'Missing url to the coa_ranking service in the Response Planner API')
        self.inputTypeExpected = 'other'
        self.inputExpected = ['coas']


    def check_response(self, response):
        status = response.status_code
        if status != 200:
            logprint("Response Error", f'{status} - {response.content}')
            self.error(f'Status: {status} - {response.content}')
        return response


    def check_inputData(self, inputData):
        for i in self.inputExpected:
            if not(i in inputData):
                logprint("Missing input:", i)
                self.error(f'Missing input:{i}')


    def generateUrl_Payload(self, inputData):
        siem_label = inputData.get('siem_label')
        initialTTCs_dict = inputData.get('initialTTC')
        initial_ids = inputData.get('initial_ids')
        containment = inputData.get('containment')
        coas = inputData.get('coas')
        json_payload = {"initialTTCs_dict":initialTTCs_dict, "coas":coas}
        base_url = f'{self.responsePlannerApi_url}{self.service_url}?'
        url = base_url
        if isinstance(siem_label, str):
            url += f'siem_label={siem_label}&'
        if isinstance(initialTTCs_dict, dict):
            url += f'initialTTCs_dict={json.dumps(initialTTCs_dict)}&'
        if isinstance(initial_ids, dict):
            url += f'initial_ids={json.dumps(initial_ids)}&'
        if isinstance(containment, bool):
            url += f'containment={json.dumps(containment)}'
        return url, json_payload


    def sendRequestToAPI(self, url, json_payload):
        logprint('Request URL', url)
        request_headers = {'Content-Type': 'application/json',
                           'accept': 'application/json'}
        response = requests.post(url, json=json_payload, headers=request_headers)
        self.check_response(response)
        return response


    def run(self):
        if self.service == 'coa_ranking':
            #get input
            if self.data_type == 'other':
                raw_inputData = self.get_param('data', None, 'Data is missing')
                inputData = json.loads(raw_inputData)
                logprint("input", inputData)
                self.check_inputData(inputData)
                url, json_payload = self.generateUrl_Payload(inputData)
                response = self.sendRequestToAPI(url, json_payload)
                logprint("Response from the Response Planner API",
                        response.content)
                self.report(json.loads(response.content))
            else:
                self.error('Invalid data type')
        else:
            self.error('Invalid service')


# Input Examples
SIEM_EVENT_EXAMPLE = "TA0001" # Initial Access
INITIAL_TTC_EXAMPLE = {"228.Read":[19,26,34],"325.NetworkRespondConnect":[23,30,40]}
CONTAINMENT_EXAMPLE = False # Bool
DEFENSE_EXAMPLE = {
    "ref": "112233",
    "defenseName": "Patched",
    "defenseInfo": "Patch application",
    "mitreRef": "M1040"
    }
DEFENSE_EXAMPLE_2 = {
    "ref": "112244",
   "defenseName": "Isolate",
   "defenseInfo": "Network Segmentation",
   "mitreRef": "M1030",
   "hosts": ["192.168.0.3", "192.168.0.10"]
    }
DEFENSE_EXAMPLE_3 = {
    "ref": "112255",
    "defenseName": "Filter",
    "defenseInfo": "Filter Network Traffic",
    "mitreRef": "M1037",
    "flows": [{"src": "192.168.0.3", "dst": "123.45.67.89"}]
    }
COA_EXAMPLE = {
    "containment": False,
    "coaTTC": {"228.Read":[89,126,534],"325.NetworkRespondConnect":[323,430,540]},
    "monetary_cost": {"1":123,"2":567,"3":789,"4":123,"5":456},
    "defenses": [DEFENSE_EXAMPLE, DEFENSE_EXAMPLE]
    }
COA_EXAMPLE_2 = {
    "containment": False,
    "coaTTC": {"228.Read":[189,826,934],"325.NetworkRespondConnect":[85,230,340]},
    "monetary_cost": {"1":144,"2":878,"3":455,"4":1423,"5":6},
    "defenses": [DEFENSE_EXAMPLE, DEFENSE_EXAMPLE]
    }
COAS_EXAMPLE = [COA_EXAMPLE, COA_EXAMPLE_2]
CONTAINMENT_COA_EXAMPLE_1 = {
    "defenses": [DEFENSE_EXAMPLE_2]
    }
CONTAINMENT_COA_EXAMPLE_2 = {
    "defenses": [DEFENSE_EXAMPLE_2, DEFENSE_EXAMPLE_3]
    }
CONTAINMENT_COAS_EXAMPLE = [CONTAINMENT_COA_EXAMPLE_1, CONTAINMENT_COA_EXAMPLE_2]
RANKING_COAS_INPUT_EXAMPLE = {"siem_event": SIEM_EVENT_EXAMPLE,
                              "initialTTC": INITIAL_TTC_EXAMPLE,
                              "coas": COAS_EXAMPLE}
SOURCE_IPS_EXAMPLE = ["192.168.0.3", "192.168.0.10"]
DESTINATION_IPS_EXAMPLE = ["123.45.67.89", "123.98.76.54"]
INCIDENT_INFO_EXAMPLE = {"sourceIPs": SOURCE_IPS_EXAMPLE,
                         "destinationIPs": DESTINATION_IPS_EXAMPLE}

# Launch the Analyzer
if __name__ == '__main__':
    SOCCRATES_RP_COA_RANKING_ANALYZER().run()
